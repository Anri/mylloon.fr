# Documentation <!-- omit in toc -->

[Here is a running example](https://www.mylloon.fr/) of a fully configured website.

---

- [Installation](#installation)
  - [Use Docker Compose](#use-docker-compose)
- [Configuration](#configuration)
  - [Global configuration](#global-configuration)
  - [Link shortener for contacts](#link-shortener-for-contacts)
- [Add content](#add-content)
  - [Index](#index)
  - [Blog](#blog)
  - [Projects](#projects)
  - [Contacts](#contacts)
  - [Courses](#courses)

# Installation

## Use Docker Compose

```yaml
version: "3.9"

services:
  ewp:
    image: git.mylloon.fr/anri/mylloon.fr:latest
    container_name: EWP
    volumes:
      - /here/your/path/config:/app/config
      - /here/your/path/data:/app/data
    ports:
      - 80:8080
    restart: unless-stopped
```

<details>
<summary>Do you want to make the site available on Tor as well?</summary>

```yaml
version: "3.9"

services:
  tor:
    image: goldy/tor-hidden-service:latest
    container_name: Website_tor
    network_mode: bridge
    links:
      - ewp
    environment:
      EWP_TOR_SERVICE_HOSTS: "80:ewp:8080"
      EWP_TOR_SERVICE_VERSION: "3"
    volumes:
      - tor-keys:/var/lib/tor/hidden_service/
    secrets:
      - source: ewp
        target: ewp
        mode: 0400
    restart: unless-stopped

  ewp:
    image: git.mylloon.fr/anri/mylloon.fr:latest
    container_name: Website
    network_mode: bridge
    hostname: ewp
    volumes:
      - /here/your/path/ewp/app/config:/app/config
      - /here/your/path/ewp/app/data:/app/data
    ports:
      - 99:8080
    restart: unless-stopped

volumes:
  tor-keys:
    driver: local

# Use https://github.com/cathugger/mkp224o to generate a key
secrets:
  ewp:
    file: /here/your/path/ewp/hidden_service/hs_ed25519_secret_key
```

You can tell the Tor Browser that you are offering a `onion` link by specifying
your address in the `config.toml` file.

```toml
onion = "http://youraddress.onion/"
```

</details>

# Configuration

## Global configuration

This file is stored at `/app/config/config.toml`

```toml
scheme = "https" # http or https (fallback to 'http' if none)
domain = "sub.domain.tld" # your domain (fallback to 'localhost' if none)
port = 8080 # port used (fallback to '8080' if none)
mail = "your.mail at host.com"
lang = "lang"
onion = "http://youraddress.onion/"
app_name = "Nickname" # fallback to 'EWP' if none
name = "Firstname"
fullname = "Fullname"
exclude_courses = []
```

## Link shortener for contacts

This file is stored at `/app/data/contacts/links.toml`

```toml
"a"   = "redirectiona.fr"
"a/e" = "redirectiona.fr/something"
"a/b" = "redirectiona.fr/amazing"
"b"   = "other_site.com"
```

This is designed by a tuple (service \* optional-scope),
separated by `/` → `service/optional_scope`

- Links will be available at `/contact/service/scope` and `/c/service/scope`
- The scope is optional
- You can't stack up scope, only one scope is allowed

> This is _by design_ to keep things easy

# Add content

Markdown files have YAML headers with `---` separator:

```
---
option: value
---

Markdown file
```

## Index

Markdown file is stored in `/app/data/index.md`

> For french clients, `/app/data/index-fr.md` will be read instead.

```
---
name: Option<String>
pronouns: Option<String>
avatar: Option<String>
avatar_caption: Option<String>
avatar_style: Option<String>
---

Index content
```

- If no `name`, the `fullname` used in the configuration will be used
- `avatar` is the link of the avatar
- `avatar_style` is either `round` (default) or `square`

## Blog

Markdown files are stored in `/app/data/blog/posts/`

```
---
title: Option<String>
date: Option<Date>
description: Option<String>
publish: Option<bool>
draft: Option<bool>
tags: Option<Vec<Tag>>
---

Post content
```

- If no `title`, the filename will be used
- `date` format is `day-month-year`
- `publish` is default to false. When false, posts are hidden from index
  but accessible.
- `draft` is default to false. When true, posts are hidden and unaccessible.

### About <!-- omit in toc -->

The file is stored at `/app/data/blog/about.md`.

> For french clients, `/app/data/blog/about-fr.md` will be read instead.

## Projects

Markdown files are stored in `/app/data/projects/apps/`

```
---
title: Option<String>
link: Option<String>
description: Option<String>
language: Option<String>
---

Project description
```

- If no `link` : the div won't be clickable and will be reported as is to the user
  (no corner-arrow)
- Note that only a handful of [`language`s are supported](./static/css/languages.css)

You can also put apps in an "Archived" category, in this case, store markdown
files in `archive` subdirectory of `apps`.

### About <!-- omit in toc -->

The file is stored at `/app/data/projects/about.md`.

> For french clients, `/app/data/projects/about-fr.md` will be read instead.

## Contacts

Markdown files are stored in `/app/data/contacts/`

```
---
title: String
custom: Option<bool>
user: "Option<String>"
link: Option<String>
newtab: Option<bool>
hide: Option<bool>
description: >
  Option<String>
---

Custom project description
```

- `custom` is default to `false`, if `true` all other metadata are ignored,
  except the `title`, and the markdown will be used instead
- When `custom` is `false` then markdown will be ignored and only metadata will
  be used to create the contact field
- `newtab` add property to open in a new tab a link
- `user` is the username used in the platform
- `description` will be rendered as HTML "title" (text will appear when cursor
  is hover the link)

Also, contacts are categorized, here is the list of the available categories:

- `socials`
- `forges`
- `others`

For example, `socials` contact files are stored in `/app/data/contacts/socials/`.

### About <!-- omit in toc -->

The file is stored at `/app/data/contacts/about.md`.

> For french clients, `/app/data/contacts/about-fr.md` will be read instead.

## Courses

Markdown files are stored in `/app/data/cours/`
