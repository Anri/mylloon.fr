import mermaid from "https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs";

window.addEventListener("load", async () => {
  mermaid.initialize({
    startOnLoad: false,
    theme: "default",
  });

  await mermaid.run({
    querySelector: ".language-mermaid",
  });
});
