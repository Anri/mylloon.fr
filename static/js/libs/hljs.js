window.addEventListener("load", () => {
  /* Configuration */
  hljs.configure({
    noHighlightRe: /^$/i,
    languageDetectRe: /\blanguage-hljs-([\w-]+)\b/i,
  });
  hljs.addPlugin(new CopyButtonPlugin());

  /* Aliases of langs */
  const aliases = {
    plaintext: ["no-highlight"], // default when language is unknown
    bash: ["fish"],
    julia: ["pseudocode"],
    x86asm: ["asm"],
  };
  for (const lang in aliases) {
    hljs.registerAliases(aliases[lang], { languageName: lang });
  }

  /* Highlight */
  hljs.highlightAll();
  hljs.initLineNumbersOnLoad();

  /* Theme */
  const dark = "dark";
  const light = "light";
  const attribute = "disabled";
  const updateTheme = (theme) => {
    {
      const elementToEnable = document.getElementById(
        `hljs-${theme.matches ? dark : light}-theme`
      );
      const elementToDisable = document.getElementById(
        `hljs-${theme.matches ? light : dark}-theme`
      );

      if (elementToEnable.hasAttribute(attribute)) {
        elementToEnable.removeAttribute(attribute);
      }
      elementToDisable.setAttribute(attribute, attribute);
    }
  };

  const darkModePreference = window.matchMedia("(prefers-color-scheme: dark)");
  updateTheme(darkModePreference);

  darkModePreference.addEventListener("change", (theme) => updateTheme(theme));
});
