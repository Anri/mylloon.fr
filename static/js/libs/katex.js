window.addEventListener("load", () => {
  const macros = {};
  for (const item of new Map(
    Object.entries({
      B: "mathbb{B}",
      N: "mathbb{N}",
      R: "mathbb{R}",
      Z: "mathbb{Z}",
      O: "Theta",
      Tau: "mathrm{T}",
      u: "mu",
      ra: "rightarrow",
      la: "leftarrow",
      RA: "Rightarrow",
      LA: "Leftarrow",
      lb: "llbracket",
      rb: "rrbracket",
    })
  )[Symbol.iterator]()) {
    const bs = "\\";
    macros[`${bs}${item[0]}`] = `${bs}${item[1]}`;
  }

  const attribute = "data-math-style";
  for (const element of document.querySelectorAll(`span[${attribute}]`)) {
    katex.render(element.textContent, element, {
      throwOnError: false,
      displayMode: element.getAttribute(attribute) === "display",
      macros: macros,
    });
  }
});
