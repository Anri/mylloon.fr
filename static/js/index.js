class Tag {
  constructor(variant, style = "") {
    this.variant = variant;
    this.style = style;
  }
}

window.addEventListener("load", () => {
  const clipping_text = `
    display: inline;
    background-clip: text;
    -webkit-background-clip: text; /* Chromium fix */
    color: transparent;
  `;
  const mono = "font-family: monospace";

  let tags = [
    new Tag("Comment fonctionne un PC 😵‍💫"),
    new Tag("undefined", mono),
    new Tag("[object Object]", mono),
    new Tag("/api/v1/love", mono),
    new Tag("/api/v1/websites", mono),
    new Tag("Peak D2 sur Valo 🤡"),
    new Tag("Nul en CSS", "font-family: 'Comic Sans MS', TSCu_Comic, cursive"),
    new Tag("anri k... caterpillar 🐛☝️"),
    new Tag(
      "Free Ukraine",
      `
        background: linear-gradient(to bottom, DodgerBlue 57%, gold 43%);
        ${clipping_text}
        text-shadow: 0px 0px 20px light-dark(var(--font-color), transparent);
      `
    ),
    new Tag("Étudiant"),
    new Tag("Rempli de malice"),
    new Tag("anri.exe", mono),
    new Tag("C'est... l'électricien"),
    new Tag("Mellow"),
    new Tag("Million"),
  ];

  const hour = new Date().getHours();
  if (hour <= 8 || hour >= 18) {
    tags = tags.concat([
      new Tag(
        "0x520",
        `
          background: linear-gradient(to bottom right, red 0%, red 50%, black 50%);
          ${clipping_text}
          text-shadow: 0px 0px 20px light-dark(transparent, var(--font-color));
        `
      ),
      new Tag("School hater"),
      new Tag(
        "Free Palestine",
        `
          background: conic-gradient(at 30% 60%, transparent 230deg, red 0, red 310deg, transparent 0),
                      linear-gradient(to bottom, black 45%, white 45%, white 67%, DarkGreen 67%);
          ${clipping_text}
          text-shadow: 0px 0px 20px var(--font-color);
        `
      ),
      new Tag(
        "#NouveauFrontPopulaire ✊",
        `
          background: linear-gradient(to bottom, #4fb26b  0%, #4fb26b 36%, \
                                                 #e62e35 36%, #e62e35 50%, \
                                                 #feeb25 50%, #feeb25 62%, \
                                                 #724e99 62%, #724e99 77%, \
                                                 #e73160 77%);
          ${clipping_text}
          text-shadow: 0px 0px 20px light-dark(var(--font-color), transparent);
        `
      ),
      new Tag("s/centre/droite/g", mono),
    ]);
  }

  const random = Math.round(Math.random() * (tags.length - 1));
  const element = document.getElementById("subname");
  element.textContent = tags[random].variant;
  element.style = tags[random].style;
});
