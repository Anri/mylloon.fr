const Mode = {
  Light: 1,
  Dark: 2,
};

/**
 * Change the svg color theme based on the mode
 * @param {Mode} mode
 */
const svgChangeTheme = (mode) => {
  for (const item of document.getElementsByTagName("img")) {
    if (!item.src.startsWith("data:image/svg+xml;base64,")) {
      // Exclude image who aren't SVG and base64 encoded
      continue;
    }

    /**
     * Convert to grayscale
     * @param {{r: number, g: number, b: number}} color
     * @returns Number between 0 and 255
     */
    const colorToGrayscale = (color) => {
      return 0.3 * color.r + 0.59 * color.g + 0.11 * color.b;
    };

    /**
     * Extract color using canvas2d
     * @param {HTMLImageElement} image Image source
     * @returns Colors represeting the image
     */
    const extractColors = (image) => {
      const canvas = document.createElement("canvas");
      canvas.width = image.naturalWidth;
      canvas.height = image.naturalHeight;
      const ctx = canvas.getContext("2d");
      ctx.drawImage(image, 0, 0);

      const imageData = ctx.getImageData(
        0,
        0,
        Math.max(1, canvas.width),
        Math.max(1, canvas.height)
      );
      const pixelData = imageData.data;

      const colors = [];
      for (let i = 0; i < pixelData.length; i += 4) {
        if (pixelData[i + 3] > 0) {
          colors.push({
            r: pixelData[i],
            g: pixelData[i + 1],
            b: pixelData[i + 2],
          });
        }
      }

      return colors;
    };

    // Extract colors
    const colors = extractColors(item);

    // Calculate the average grayscale value
    const grayscaleValues = colors.map(colorToGrayscale);
    const totalGrayscale = grayscaleValues.reduce((acc, val) => acc + val, 0);
    const averageGrayscale = totalGrayscale / grayscaleValues.length;

    const treshold = 128;

    const style = "filter: ";
    const dim = "brightness(0.8) contrast(1.2)";

    if (averageGrayscale < treshold && mode === Mode.Dark) {
      item.style = style + dim + " invert(1);";
      continue;
    }

    if (averageGrayscale > treshold && mode === Mode.Light) {
      item.style = style + "invert(1);";
      continue;
    }

    if (mode === Mode.Dark) {
      item.style = style + `${dim};`;
      continue;
    }

    item.style = "";
  }
};

/**
 * Replace base64 urls of embeded PDFs into blob
 */
const blobifyPdfs = () => {
  const pdfContentType = "application/pdf";
  for (const item of document.getElementsByTagName("embed")) {
    if (!item.src.startsWith(`data:${pdfContentType};base64,`)) {
      // Exclude embed who are not PDFs encoded via base64
      continue;
    }

    /**
     * Convert Base64 data to a blob
     * @param {String} b64Data Encoded data
     * @param {Number} sliceSize Size of the slices
     * @returns Blob representing the data
     */
    const base64ToBlob = async (b64Data, sliceSize = 512) => {
      const byteCharacters = atob(b64Data);
      const byteArrays = [];
      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      return new Blob(byteArrays, { type: pdfContentType });
    };

    base64ToBlob(item.src.split(",")[1]).then((blob) => {
      const newUrl = URL.createObjectURL(blob);
      item.src = newUrl;

      const link = document.createElement("a");
      link.href = newUrl;
      link.target = "_blank";
      link.textContent = "Ouvrir le PDF dans un nouvel onglet";
      link.style.display = "grid";
      link.style.justifyContent = "center";
      item.insertAdjacentElement("afterend", link);
    });
  }
};

window.addEventListener("DOMContentLoaded", () => {
  // Turn Base64 PDFs into blobs
  blobifyPdfs();
});

window.addEventListener("load", () => {
  // Fix SVG images
  svgChangeTheme(
    window.matchMedia("(prefers-color-scheme: dark)").matches
      ? Mode.Dark
      : Mode.Light
  );
});

window
  .matchMedia("(prefers-color-scheme: dark)")
  .addEventListener("change", (event) =>
    svgChangeTheme(event.matches ? Mode.Dark : Mode.Light)
  );
