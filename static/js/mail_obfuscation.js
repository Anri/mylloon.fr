window.addEventListener("load", () => {
  Array.from(document.getElementsByClassName("at")).forEach((elem) => {
    const a = elem.parentElement;
    const span = elem.previousElementSibling;

    // Replace (at) by @
    elem.outerHTML = "@";

    // Correct text
    const data = span.getAttribute("title");
    data.length > 0 ? (a.innerHTML = data) : (a.style = "hyphens: none;");

    // Change link
    const href = a.getAttribute("href");
    a.setAttribute("href", href.replace(" at ", "@"));
  });
});
