# syntax=docker/dockerfile:1

FROM rust:bullseye AS builder
WORKDIR /usr/src/ewp
COPY . .

RUN cargo install --path .


FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

COPY --from=builder /usr/local/cargo/bin/ewp /app/ewp
COPY --from=builder /usr/src/ewp/LICENSE /app/LICENSE
COPY --from=builder /usr/src/ewp/README.md /app/README.md
COPY --from=builder /usr/src/ewp/static /app/static
COPY --from=builder /usr/src/ewp/templates /app/templates

RUN mkdir /app/config \
    && touch /app/config/config.toml

CMD [ "./ewp" ]
