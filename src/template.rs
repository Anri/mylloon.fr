use ramhorns::{Content, Ramhorns};

use crate::utils::misc::Lang;

/// Structure used in the config variable of the app
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Template {
    /// Root directory where templates are stored
    pub directory: String,
    /// App name
    pub app_name: String,
    /// URL of app
    pub url: String,
    /// Name of user
    pub name: Option<String>,
}

/// Structure used by /routes/*.rs
#[derive(Debug, Default)]
pub struct InfosPage {
    /// Title
    pub title: Option<String>,
    /// Description
    pub desc: Option<String>,
    /// Keywords
    pub kw: Option<String>,
}

#[allow(clippy::struct_excessive_bools)]
/// Information on what page the user is currently
#[derive(Content, Debug, Default)]
pub struct NavBar {
    pub index: bool,
    pub blog: bool,
    pub portfolio: bool,
    pub contact: bool,
    pub contrib: bool,
    pub cours: bool,
}

/// Final structure given to template
#[derive(Content, Debug)]
struct DataPage<T> {
    /// App name
    app_name: String,
    /// App URL
    url: String,
    /// Title of the page
    page_title: Option<String>,
    /// Description of the page
    page_desc: Option<String>,
    /// Keywords of the the page
    page_kw: Option<String>,
    /// Author's name
    page_author: Option<String>,
    /// Language used
    lang: String,
    /// Data needed to render the page
    data: T,
}

impl Template {
    pub fn render<C: Content>(
        &self,
        template: &str,
        data: C,
        info: InfosPage,
        lang: Option<String>,
    ) -> String {
        let mut templates: Ramhorns = Ramhorns::lazy(&self.directory).unwrap();
        let tplt = templates.from_file(template).unwrap();

        tplt.render(&DataPage {
            app_name: self.app_name.clone(),
            url: self.url.clone(),
            page_title: info.title,
            page_desc: info.desc,
            page_kw: info.kw,
            page_author: self.name.clone(),
            lang: lang.unwrap_or(Lang::default()),
            data,
        })
    }
}
