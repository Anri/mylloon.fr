use reqwest::{header::ACCEPT, Error};
use serde::Deserialize;

use crate::utils::misc::get_reqwest_client;

#[derive(Debug, Deserialize)]
struct GithubResponse {
    items: Vec<GithubProject>,
}

#[derive(Debug, Deserialize)]
struct GithubProject {
    repository_url: String,
    number: u32,
    title: String,
    state: String,
    pull_request: GithubPullRequest,
}

#[derive(Debug, Deserialize)]
struct GithubPullRequest {
    html_url: String,
    merged_at: Option<String>,
}

#[derive(Clone, Copy, Debug)]
pub enum ProjectState {
    Closed = 0,
    Open = 1,
    Merged = 2,
}

impl TryFrom<u8> for ProjectState {
    type Error = ();

    fn try_from(orig: u8) -> Result<Self, Self::Error> {
        match orig {
            0 => Ok(Self::Closed),
            1 => Ok(Self::Open),
            2 => Ok(Self::Merged),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
pub struct Project {
    pub name: String,
    pub url: String,
    pub status: ProjectState,
    pub title: String,
    pub id: u32,
    pub contrib_url: String,
}

pub async fn fetch_pr() -> Result<Vec<Project>, Error> {
    // Result<GithubResponse, Error>
    let client = get_reqwest_client();

    let resp = client
        .get("https://api.github.com/search/issues?q=is:pr%20author:Mylloon")
        .header(ACCEPT, "application/vnd.github.text-match+json")
        .send()
        .await?
        .json::<GithubResponse>()
        .await?;

    let mut list = vec![];
    resp.items.iter().for_each(|p| {
        list.push(Project {
            name: p.repository_url.split('/').last().unwrap().into(),
            url: p.repository_url.clone(),
            status: if p.pull_request.merged_at.is_none() {
                if p.state == "closed" {
                    ProjectState::Closed
                } else {
                    ProjectState::Open
                }
            } else {
                ProjectState::Merged
            },
            title: p.title.clone(),
            id: p.number,
            contrib_url: p.pull_request.html_url.clone(),
        });
    });

    Ok(list)
}
