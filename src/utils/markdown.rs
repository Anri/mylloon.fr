use base64::engine::general_purpose;
use base64::Engine;
use comrak::nodes::{AstNode, NodeValue};
use comrak::{format_html, parse_document, Arena, ComrakOptions, ListStyleType, Options};
use lol_html::html_content::ContentType;
use lol_html::{element, rewrite_str, HtmlRewriter, RewriteStrSettings, Settings};
use ramhorns::Content;
use std::fmt::Debug;
use std::fs;
use std::path::Path;
use std::sync::Arc;

use crate::utils::metadata::MType;
use crate::utils::misc::remove_first_letter;

use super::metadata::{get, MFile, Metadata};

/// File description
#[derive(Content, Debug, Clone)]
pub struct File {
    pub metadata: Metadata,
    pub content: String,
}

#[derive(Hash, PartialEq, Eq, Clone)]
pub struct FilePath {
    pub base: String,
    pub path: String,
}

impl std::fmt::Display for FilePath {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.path.is_empty() {
            return write!(f, "{}", self.base);
        }
        if self.base.is_empty() {
            return write!(f, "{}", self.path);
        }

        write!(f, "{}/{}", self.base, self.path)
    }
}

impl FilePath {
    pub fn from(&self, fullpath: &str) -> Self {
        Self {
            base: self.base.clone(),
            path: fullpath.to_owned().split_off(self.base.len()),
        }
    }
}

/// Options used for parser and compiler MD --> HTML
pub fn get_options(path: Option<FilePath>, metadata_type: MType) -> ComrakOptions<'static> {
    comrak::Options {
        extension: comrak::ExtensionOptions::builder()
            .strikethrough(true)
            .tagfilter(true)
            .table(true)
            .autolink(true)
            .tasklist(true)
            .superscript(true)
            .header_ids(path_to_hid(path.as_ref()))
            .footnotes(true)
            .description_lists(true)
            .front_matter_delimiter("---".into())
            .multiline_block_quotes(true)
            .math_dollars(true)
            .underline(true)
            .maybe_link_url_rewriter(match metadata_type {
                MType::Cours => Some(Arc::new(move |url: &str| {
                    // Exclude external links
                    if [
                        "://",
                        "mailto:",
                        "magnet:",
                        "ftp:",
                        "tel:",
                        "irc:",
                        "geo:",
                        "ftps:",
                        "im:",
                        "ircs:",
                        "bitcoin:",
                        "matrix:",
                        "mms:",
                        "news:",
                        "nntp:",
                        "openpgp4fpr:",
                        "sftp:",
                        "sip:",
                        "sms:",
                        "smsto:",
                        "ssh:",
                        "urn:",
                        "webcal:",
                        "wtai:",
                        "xmpp:",
                    ]
                    .iter()
                    .any(|&item| url.contains(item))
                    {
                        return String::from(url);
                    }

                    let file = if url.starts_with("./") {
                        url.to_owned().split_off(2)
                    } else {
                        url.to_owned()
                    };

                    match &path {
                        Some(p) => {
                            let mut parts = p.path.split('/').collect::<Vec<_>>();
                            parts.pop();
                            parts.push(&file);

                            format!("/cours?q={}", parts.join("/"))
                        }
                        None => format!("/cours?q={file}"),
                    }
                })),
                _ => None,
            })
            .build(),
        parse: comrak::ParseOptions::builder()
            .smart(true)
            .default_info_string("plaintext".into())
            .relaxed_tasklist_matching(true)
            .relaxed_autolinks(true)
            .build(),
        render: comrak::RenderOptions::builder()
            .full_info_string(true)
            .unsafe_(true)
            .list_style(ListStyleType::Dash)
            .ignore_setext(true)
            .ignore_empty_links(true)
            .gfm_quirks(true)
            .build(),
    }
}

/// Transform the path to something usable for header IDs
fn path_to_hid(path: Option<&FilePath>) -> String {
    match path {
        Some(fp) => {
            format!(
                "{}-",
                fp.path
                    .get(..fp.path.len() - 3)
                    .unwrap_or_default()
                    .replace([' ', '/'], "-")
                    .to_lowercase()
            )
        }
        None => String::new(),
    }
}

/// Resize images if needed
fn custom_img_size(html: &str) -> String {
    rewrite_str(
        html,
        RewriteStrSettings {
            element_content_handlers: vec![element!("img[alt]", |el| {
                let alt = el.get_attribute("alt").unwrap();
                let possible_piece = alt.split('|').collect::<Vec<&str>>();

                if possible_piece.len() > 1 {
                    let data = possible_piece.last().unwrap().trim();

                    // Remove the dimension data from the alt
                    let new_alt = possible_piece.first().unwrap().trim_end();

                    if let Some(dimension) = data.split_once('x') {
                        // Width and height precised
                        if dimension.0.parse::<i32>().is_ok() && dimension.1.parse::<i32>().is_ok()
                        {
                            el.set_attribute("width", dimension.0).unwrap();
                            el.set_attribute("height", dimension.1).unwrap();
                            if new_alt.is_empty() {
                                el.remove_attribute("alt");
                            } else {
                                el.set_attribute("alt", new_alt).unwrap();
                            }
                        }
                    } else {
                        // Only width precised
                        if data.parse::<i32>().is_ok() {
                            el.set_attribute("width", data).unwrap();
                            if new_alt.is_empty() {
                                el.remove_attribute("alt");
                            } else {
                                el.set_attribute("alt", new_alt).unwrap();
                            }
                        }
                    }
                }

                Ok(())
            })],
            ..RewriteStrSettings::default()
        },
    )
    .unwrap()
}

fn fix_headers_ids(html: &str, path: Option<&FilePath>) -> String {
    rewrite_str(
        html,
        RewriteStrSettings {
            element_content_handlers: vec![element!(
                "a:not([data-footnote-ref]):not(.footnote-backref)[href^='#']",
                |el| {
                    let id = el.get_attribute("id").unwrap_or(
                        path_to_hid(path) + remove_first_letter(&el.get_attribute("href").unwrap()),
                    );
                    el.set_attribute("href", &format!("#{id}")).unwrap();

                    Ok(())
                }
            )],
            ..RewriteStrSettings::default()
        },
    )
    .unwrap()
}

/// Fix local images to base64 and integration of markdown files
fn fix_images_and_integration(
    path: &FilePath,
    html: &str,
    metadata_type: MType,
    recursive: bool,
) -> (String, Metadata) {
    let mut metadata = Metadata {
        info: MFile::default(),
        math: false,
        mermaid: false,
        syntax_highlight: false,
        mail_obfsucated: false,
    };

    // Collection of any additional metadata
    let mut additional_metadata = Vec::new();

    let result = rewrite_str(
        html,
        RewriteStrSettings {
            element_content_handlers: vec![element!("img", |el| {
                if let Some(src) = el.get_attribute("src") {
                    let path_str = path.to_string();
                    let img_src = Path::new(&path_str).parent().unwrap();
                    let img_path = urlencoding::decode(img_src.join(src).to_str().unwrap())
                        .unwrap()
                        .to_string();

                    if let Ok(file_contents) = fs::read(&img_path) {
                        let mime = mime_guess::from_path(&img_path).first_or_octet_stream();
                        if recursive && mime == "text/markdown" {
                            let file_str = String::from_utf8_lossy(&file_contents).into_owned();

                            // Find a ok-ish path
                            let new_path = img_path.split('/');
                            let mut options = get_options(
                                Some(FilePath {
                                    base: new_path
                                        .clone()
                                        .take(new_path.clone().count() - 1)
                                        .collect::<Vec<_>>()
                                        .join("/"),
                                    path: new_path.last().unwrap_or("").to_string(),
                                }),
                                metadata_type,
                            );

                            options.extension.footnotes = false;
                            let data = read_md(
                                &path.from(&img_path),
                                &file_str,
                                metadata_type,
                                Some(options),
                                false,
                            );
                            el.replace(&data.content, ContentType::Html);

                            // Store the metadata for later merging
                            additional_metadata.push(data.metadata);
                        } else {
                            let image = general_purpose::STANDARD.encode(&file_contents);
                            el.set_attribute("src", &format!("data:{mime};base64,{image}"))
                                .unwrap();
                        }
                    }
                }
                Ok(())
            })],
            ..RewriteStrSettings::default()
        },
    )
    .unwrap();

    // Merge all collected metadata
    for additional in additional_metadata {
        metadata.merge(&additional);
    }

    (result, metadata)
}

/// Transform markdown string to File structure
pub fn read_md(
    path: &FilePath,
    raw_text: &str,
    metadata_type: MType,
    options: Option<Options>,
    recursive: bool,
) -> File {
    let arena = Arena::new();

    let mut opt = options.map_or_else(
        || get_options(Some(path.clone()), metadata_type),
        |specific_opt| specific_opt,
    );
    let root = parse_document(&arena, raw_text, &opt);

    // Find metadata
    let metadata = get(root, metadata_type);

    // Update comrak render properties
    opt.render.hardbreaks = metadata.hardbreaks;

    let mermaid_name = "mermaid";
    hljs_replace(root, mermaid_name);

    // Convert to HTML
    let mut html = vec![];
    format_html(root, &opt, &mut html).unwrap();

    let mut html_content = String::from_utf8(html).unwrap();

    let children_metadata;
    let mail_obfsucated;
    (html_content, children_metadata) =
        fix_images_and_integration(path, &html_content, metadata_type, recursive);
    html_content = custom_img_size(&html_content);
    html_content = fix_headers_ids(&html_content, Some(path));
    (html_content, mail_obfsucated) = mail_obfuscation(&html_content);

    let mut final_metadata = Metadata {
        info: metadata,
        mermaid: check_mermaid(root, mermaid_name),
        syntax_highlight: check_code(root, &[mermaid_name.into()]),
        math: check_math(&html_content),
        mail_obfsucated,
    };
    final_metadata.merge(&children_metadata);

    File {
        metadata: final_metadata,
        content: html_content,
    }
}

/// Check whether mermaid diagrams are in the AST
fn check_mermaid<'a>(root: &'a AstNode<'a>, mermaid_str: &str) -> bool {
    root.descendants()
        .any(|node| match &node.data.borrow().value {
            // Check if code of block define a mermaid diagram
            NodeValue::CodeBlock(code_block) => code_block.info == mermaid_str,
            _ => false,
        })
}

/// Check if code is in the AST
fn check_code<'a>(root: &'a AstNode<'a>, blacklist: &[String]) -> bool {
    root.descendants()
        .any(|node| match &node.data.borrow().value {
            // Detect blocks of code where the lang isn't in the blacklist
            NodeValue::CodeBlock(code_block) => !blacklist.contains(&code_block.info),
            _ => false,
        })
}

/// Check if html contains maths
fn check_math(html: &str) -> bool {
    let mut math_detected = false;

    let _ = HtmlRewriter::new(
        Settings {
            element_content_handlers: vec![element!("span[data-math-style]", |_| {
                math_detected = true;

                Ok(())
            })],
            ..Settings::default()
        },
        |_: &[u8]| {},
    )
    .write(html.as_bytes());

    math_detected
}

/// Change class of languages for hljs detection
fn hljs_replace<'a>(root: &'a AstNode<'a>, mermaid_str: &str) {
    root.descendants().for_each(|node| {
        if let NodeValue::CodeBlock(ref mut block) = &mut node.data.borrow_mut().value {
            if block.info != mermaid_str {
                block.info = format!("hljs-{}", block.info);
            }
        }
    });
}

/// Obfuscate email if email found
fn mail_obfuscation(html: &str) -> (String, bool) {
    let mut modified = false;

    let data_attr = "title";

    // Modify HTML for mails
    let new_html = rewrite_str(
        html,
        RewriteStrSettings {
            element_content_handlers: vec![element!("a[href^='mailto:']", |el| {
                modified = true;

                // Get mail address
                let link = el.get_attribute("href").unwrap();
                let (uri, mail) = &link.split_at(7);
                let (before, after) = mail.split_once('@').unwrap();

                // Preserve old data and add obfuscated mail address
                el.prepend(&format!("<span {data_attr}='"), ContentType::Html);
                let modified_mail = format!("'></span>{before}<span class='at'>(at)</span>{after}");
                el.append(&modified_mail, ContentType::Html);

                // Change href
                Ok(el.set_attribute("href", &format!("{uri}{before} at {after}"))?)
            })],
            ..RewriteStrSettings::default()
        },
    )
    .unwrap();

    if modified {
        // Remove old data email if exists
        (
            rewrite_str(
                &new_html,
                RewriteStrSettings {
                    element_content_handlers: vec![element!(
                        &format!("a[href^='mailto:'] > span[{data_attr}]"),
                        |el| {
                            Ok(el.set_attribute(
                                data_attr,
                                // Remove mails
                                el.get_attribute(data_attr)
                                    .unwrap()
                                    .split_whitespace()
                                    .filter(|word| !word.contains('@'))
                                    .collect::<Vec<&str>>()
                                    .join(" ")
                                    .trim(),
                            )?)
                        }
                    )],
                    ..RewriteStrSettings::default()
                },
            )
            .unwrap(),
            modified,
        )
    } else {
        (new_html, modified)
    }
}
