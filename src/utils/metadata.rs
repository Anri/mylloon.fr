use crate::utils::date::Date;
use comrak::nodes::{AstNode, NodeValue};
use ramhorns::Content;
use serde::{Deserialize, Deserializer};
use std::fmt::Debug;

/// Metadata for blog posts
#[derive(Content, Clone, Debug, Default, Deserialize)]
pub struct FileMetadataBlog {
    pub hardbreaks: Option<bool>,
    pub title: Option<String>,
    pub date: Option<Date>,
    pub description: Option<String>,
    pub publish: Option<bool>,
    pub draft: Option<bool>,
    pub tags: Option<Vec<Tag>>,
    pub toc: Option<bool>,
}

/// A tag, related to post blog
#[derive(Content, Debug, Clone)]
pub struct Tag {
    pub name: String,
}

impl<'a> Deserialize<'a> for Tag {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'a>,
    {
        match <&str>::deserialize(deserializer) {
            Ok(s) => match serde_yml::from_str(s) {
                Ok(tag) => Ok(Self { name: tag }),
                Err(e) => Err(serde::de::Error::custom(e)),
            },
            Err(e) => Err(e),
        }
    }
}

/// Metadata for contact entry
#[derive(Content, Debug, Default, Deserialize, Clone)]
pub struct FileMetadataContact {
    pub title: String,
    pub custom: Option<bool>,
    pub user: Option<String>,
    pub link: Option<String>,
    pub newtab: Option<bool>,
    pub description: Option<String>,
    pub hide: Option<bool>,
}

/// Metadata for index page
#[derive(Content, Debug, Default, Deserialize, Clone)]
pub struct FileMetadataIndex {
    pub name: Option<String>,
    pub pronouns: Option<String>,
    pub avatar: Option<String>,
    pub avatar_caption: Option<String>,
    pub avatar_style: Option<String>,
}

/// Metadata for portfolio cards
#[derive(Content, Debug, Default, Deserialize, Clone)]
pub struct FileMetadataPortfolio {
    pub title: Option<String>,
    pub link: Option<String>,
    pub description: Option<String>,
    pub language: Option<String>,
    pub new: Option<bool>,
}

/// List of available metadata types
#[derive(Hash, PartialEq, Eq, Clone, Copy)]
pub enum MType {
    Blog,
    Contact,
    Cours,
    Generic,
    Index,
    Portfolio,
}

/// Structure who holds all the metadata the file have
/// Usually all fields are None except one
#[derive(Content, Debug, Default, Deserialize, Clone)]
pub struct MFile {
    pub hardbreaks: bool,
    pub blog: Option<FileMetadataBlog>,
    pub contact: Option<FileMetadataContact>,
    pub index: Option<FileMetadataIndex>,
    pub portfolio: Option<FileMetadataPortfolio>,
}

#[allow(clippy::struct_excessive_bools)]
/// Global metadata
#[derive(Content, Debug, Clone, Default)]
pub struct Metadata {
    pub info: MFile,
    pub math: bool,
    pub mermaid: bool,
    pub syntax_highlight: bool,
    pub mail_obfsucated: bool,
}

impl Metadata {
    /// Update current metadata boolean fields, keeping true ones
    pub fn merge(&mut self, other: &Self) {
        self.math = self.math || other.math;
        self.mermaid = self.mermaid || other.mermaid;
        self.syntax_highlight = self.syntax_highlight || other.syntax_highlight;
    }
}

/// Deserialize metadata based on a type
fn deserialize_metadata<T: Default + serde::de::DeserializeOwned>(text: &str) -> T {
    serde_yml::from_str(text.trim().trim_matches(&['-'] as &[_])).unwrap_or_default()
}

/// Fetch metadata from AST
pub fn get<'a>(root: &'a AstNode<'a>, mtype: MType) -> MFile {
    root.children()
        .map(|node| match &node.data.borrow().value {
            // Extract metadata from frontmatter
            NodeValue::FrontMatter(text) => match mtype {
                MType::Blog => {
                    let metadata = deserialize_metadata::<FileMetadataBlog>(text);
                    MFile {
                        blog: Some(metadata.clone()),
                        hardbreaks: metadata.hardbreaks.unwrap_or_default(),
                        ..MFile::default()
                    }
                }
                MType::Contact => {
                    let mut metadata = deserialize_metadata::<FileMetadataContact>(text);
                    // Trim descriptions
                    if let Some(desc) = &mut metadata.description {
                        desc.clone_from(&desc.trim().into());
                    }
                    MFile {
                        contact: Some(metadata),
                        ..MFile::default()
                    }
                }
                MType::Generic | MType::Cours => MFile {
                    hardbreaks: deserialize_metadata(text),
                    ..MFile::default()
                },
                MType::Index => MFile {
                    index: Some(deserialize_metadata(text)),
                    ..MFile::default()
                },
                MType::Portfolio => MFile {
                    portfolio: Some(deserialize_metadata(text)),
                    ..MFile::default()
                },
            },
            _ => MFile {
                hardbreaks: true,
                ..MFile::default()
            },
        })
        .next()
        .unwrap_or_default()
}
