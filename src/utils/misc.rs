use std::{fs, os::unix::fs::MetadataExt, path::Path};

use actix_web::{
    http::{
        header::{self, ContentType, HeaderMap, TryIntoHeaderValue},
        StatusCode,
    },
    HttpRequest, HttpResponse, Responder,
};
use base64::{engine::general_purpose, Engine};
use cached::proc_macro::cached;
use mime_guess::mime;
use reqwest::Client;

use crate::config::FileConfiguration;

use super::{
    markdown::{read_md, File, FilePath},
    metadata::{MType, Metadata},
};

#[cached]
pub fn get_reqwest_client() -> Client {
    Client::builder()
        .user_agent(format!("EWP/{}", env!("CARGO_PKG_VERSION")))
        .build()
        .unwrap()
}

/// Get URL of the app
pub fn get_url(fc: FileConfiguration) -> String {
    /* let port = match fc.scheme.as_deref() {
        Some("https") if fc.port == Some(443) => String::new(),
        Some("http") if fc.port == Some(80) => String::new(),
        _ => format!(":{}", fc.port.unwrap()),
    }; */

    format!("{}://{}", fc.scheme.unwrap(), fc.domain.unwrap())
}

/// Make a list of keywords
pub fn make_kw(list: &[&str]) -> String {
    list.join(", ")
}

/// Send HTML file
pub struct Html(pub String);
impl Responder for Html {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        let mut res = HttpResponse::with_body(StatusCode::OK, self.0);
        res.headers_mut().insert(
            header::CONTENT_TYPE,
            ContentType::html().try_into_value().unwrap(),
        );
        res
    }
}

/// Read a file localized, fallback to default file if localized file isn't found
pub fn read_file_fallback(
    filename: FilePath,
    expected_file: MType,
    lang: &Lang,
) -> (Option<File>, String) {
    match read_file(filename.clone(), expected_file, Some(lang.clone())) {
        None => (
            read_file(filename, expected_file, None),
            Lang::English.to_string(),
        ),
        data => (data, lang.to_string()),
    }
}

/// Read a file
pub fn read_file(filename: FilePath, expected_file: MType, lang: Option<Lang>) -> Option<File> {
    reader(filename, expected_file, lang.unwrap_or(Lang::English))
}

#[cached(time = 600)]
fn reader(filename: FilePath, expected_file: MType, lang: Lang) -> Option<File> {
    let as_str = match lang {
        Lang::French => {
            let str = filename.to_string();
            let mut parts = str.split('.').collect::<Vec<_>>();
            let extension = parts.pop().unwrap_or("");
            let filename = parts.join(".");
            &format!("{filename}-fr.{extension}")
        }
        Lang::English => &filename.to_string(),
    };
    let path = Path::new(as_str);

    if let Ok(metadata) = path.metadata() {
        // Taille maximale : 30M
        if metadata.size() > 30 * 1000 * 1000 {
            return None;
        }
    }

    path.extension().and_then(|ext| {
        match mime_guess::from_ext(ext.to_str().unwrap_or_default()).first_or_text_plain() {
            mime if mime == mime::APPLICATION_PDF => {
                fs::read(as_str).map_or(None, |bytes| Some(read_pdf(bytes)))
            }
            mime if mime.type_() == mime::IMAGE => {
                fs::read(as_str).map_or(None, |bytes| Some(read_img(bytes, &mime)))
            }
            _ => fs::read_to_string(as_str).map_or(None, |text| {
                Some(read_md(&filename, &text, expected_file, None, true))
            }),
        }
    })
}

fn read_pdf(data: Vec<u8>) -> File {
    let pdf = general_purpose::STANDARD.encode(data);

    File {
        metadata: Metadata::default(),
        content: format!(
            r#"<embed
                src="data:{};base64,{pdf}"
                style="width: 100%; height: 79vh";
            >"#,
            mime::APPLICATION_PDF
        ),
    }
}

fn read_img(data: Vec<u8>, mime: &mime::Mime) -> File {
    let image = general_purpose::STANDARD.encode(data);

    File {
        metadata: Metadata::default(),
        content: format!("<img src='data:{mime};base64,{image}'>"),
    }
}

/// Remove the first character of a string
pub fn remove_first_letter(s: &str) -> &str {
    s.chars().next().map(|c| &s[c.len_utf8()..]).unwrap()
}

#[derive(Hash, PartialEq, Eq, Clone)]
pub enum Lang {
    French,
    English,
}

impl Lang {
    pub fn default() -> String {
        Lang::French.to_string()
    }
}

impl std::fmt::Display for Lang {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Lang::French => write!(f, "fr"),
            Lang::English => write!(f, "en"),
        }
    }
}

/// Get the browser language
pub fn lang(headers: &HeaderMap) -> Lang {
    headers
        .get("Accept-Language")
        .and_then(|lang| lang.to_str().ok())
        .and_then(|lang| {
            ["fr", "fr-FR"]
                .into_iter()
                .any(|i| lang.contains(i))
                .then_some(Lang::French)
        })
        .unwrap_or(Lang::English)
}
