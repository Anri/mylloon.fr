use chrono::{Datelike, NaiveDate};
use ramhorns::Content;
use serde::{Deserialize, Deserializer};

#[derive(Content, Clone, Default, Debug)]
pub struct Date {
    pub day: u32,
    pub month: u32,
    pub year: i32,
}

impl<'de> Deserialize<'de> for Date {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        match <&str>::deserialize(deserializer) {
            Ok(s) => match NaiveDate::parse_from_str(s, "%d-%m-%Y") {
                Ok(date) => Ok(Self {
                    day: date.day(),
                    month: date.month(),
                    year: date.year(),
                }),
                Err(e) => Err(serde::de::Error::custom(e)),
            },
            Err(e) => Err(e),
        }
    }
}
