use actix_web::{get, web, HttpResponse, Responder};
use cached::proc_macro::once;
use ramhorns::Content;

use crate::{
    config::Config,
    logic::portfolio::{get_langs, Language},
    template::InfosPage,
};

#[get("/css/languages-defs.css")]
pub async fn languages(config: web::Data<Config>) -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/css")
        .body(build_languages(config.get_ref().to_owned()))
}

#[derive(Content, Debug)]
struct LanguagesTemplate {
    langs: Vec<Language>,
    max_size: usize,
}

#[once]
fn build_languages(config: Config) -> String {
    let langs = get_langs();
    config.tmpl.render(
        "static/languages-defs.css",
        LanguagesTemplate {
            langs: langs.clone(),
            max_size: langs.iter().fold(0, |acc, x| {
                if x.name.len() > acc {
                    x.name.len()
                } else {
                    acc
                }
            }) / 2,
        },
        InfosPage::default(),
        None,
    )
}
