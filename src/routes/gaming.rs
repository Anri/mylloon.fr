use actix_web::{get, Responder};

#[get("/gaming")]
pub async fn page() -> impl Responder {
    // Liste de mes comptes gaming, de mon setup, de mes configs de jeu, etc.
    actix_web::web::Redirect::to("/")
}
