use actix_web::{get, Responder};

#[get("/cv")]
pub async fn page() -> impl Responder {
    // Génération du CV depuis un fichier externe TOML ?
    // Cf. https://github.com/sinaatalay/rendercv
    // Faudrait une version HTML, et une version PDF
    // Aussi la possibilité d'avoir une version "full"
    // et une version "censuré" pour éviter de mettre trop
    // d'infos persos sur le web
    // Cf. https://git.mylloon.fr/Anri/cvr
    actix_web::web::Redirect::to("/")
}
