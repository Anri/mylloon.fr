use crate::{config::Config, template::InfosPage, utils::misc::get_url};
use actix_web::{get, http::header::ContentType, routes, web, HttpResponse, Responder};
use cached::proc_macro::once;
use ramhorns::Content;

#[routes]
#[get("/.well-known/security.txt")]
#[get("/security.txt")]
pub async fn security(config: web::Data<Config>) -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body(build_securitytxt(config.get_ref().to_owned()))
}

#[derive(Content, Debug)]
struct SecurityTemplate {
    url: String,
    contact: String,
    pref_lang: String,
}

#[once(time = 60)]
fn build_securitytxt(config: Config) -> String {
    config.tmpl.render(
        "security.txt",
        SecurityTemplate {
            url: format!("{}/.well-known/security.txt", get_url(config.fc.clone())),
            contact: config.fc.mail.unwrap_or_default(),
            pref_lang: config.fc.lang.unwrap_or_default(),
        },
        InfosPage::default(),
        None,
    )
}

#[get("/humans.txt")]
pub async fn humans(config: web::Data<Config>) -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body(build_humanstxt(config.get_ref().to_owned()))
}

#[derive(Content, Debug)]
struct HumansTemplate {
    contact: String,
    lang: String,
    name: String,
}

#[once(time = 60)]
fn build_humanstxt(config: Config) -> String {
    config.tmpl.render(
        "humans.txt",
        HumansTemplate {
            contact: config.fc.mail.unwrap_or_default(),
            lang: config.fc.lang.unwrap_or_default(),
            name: config.fc.fullname.unwrap_or_default(),
        },
        InfosPage::default(),
        None,
    )
}

#[get("/robots.txt")]
pub async fn robots() -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body(build_robotstxt())
}

#[once]
fn build_robotstxt() -> String {
    "User-agent: * Allow: /".into()
}

#[get("/app.webmanifest")]
pub async fn webmanifest(config: web::Data<Config>) -> impl Responder {
    HttpResponse::Ok()
        .content_type(ContentType("application/manifest+json".parse().unwrap()))
        .body(build_webmanifest(config.get_ref().to_owned()))
}

#[derive(Content, Debug)]
struct WebManifestTemplate {
    name: String,
    description: String,
    url: String,
}

#[once(time = 60)]
fn build_webmanifest(config: Config) -> String {
    config.tmpl.render(
        "app.webmanifest",
        WebManifestTemplate {
            name: config.fc.clone().app_name.unwrap(),
            description: "Easy WebPage generator".to_owned(),
            url: get_url(config.fc),
        },
        InfosPage::default(),
        None,
    )
}
