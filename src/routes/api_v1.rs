use actix_web::{get, HttpResponse, Responder};

use crate::logic::api::v1;

#[get("/love")]
pub async fn love() -> impl Responder {
    HttpResponse::Ok().json(v1::love::json())
}

#[get("/backtofrance")]
pub async fn btf() -> impl Responder {
    HttpResponse::Ok().json(v1::backtofrance::json())
}

#[get("/websites")]
pub async fn websites() -> impl Responder {
    HttpResponse::Ok().json(v1::websites::tuple())
}
