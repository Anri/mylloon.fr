use actix_web::{get, Responder};

#[get("/memorial")]
pub async fn page() -> impl Responder {
    // Memorial? J'espere ne jamais faire cette page lol
    actix_web::web::Redirect::to("/")
}
