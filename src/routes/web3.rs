use actix_web::{get, web, Responder};
use cached::proc_macro::once;

use crate::{
    config::Config,
    template::InfosPage,
    utils::misc::{make_kw, Html},
};

#[get("/web3")]
pub async fn page(config: web::Data<Config>) -> impl Responder {
    Html(build_page(config.get_ref().to_owned()))
}

#[once]
fn build_page(config: Config) -> String {
    config.tmpl.render(
        "web3.html",
        (),
        InfosPage {
            title: Some("Mylloon".into()),
            desc: Some("Coin reculé de l'internet".into()),
            kw: Some(make_kw(&["web3", "blockchain", "nft", "ai"])),
        },
        None,
    )
}
