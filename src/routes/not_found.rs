use actix_web::{web, Responder};
use cached::proc_macro::once;
use ramhorns::Content;

use crate::{
    config::Config,
    template::{InfosPage, NavBar},
    utils::misc::{get_url, Html},
};

pub async fn page(config: web::Data<Config>) -> impl Responder {
    Html(build_page(config.get_ref().to_owned()))
}

#[derive(Content, Debug)]
struct NotFoundTemplate {
    navbar: NavBar,
    www: String,
    onion: Option<String>,
}

#[once(time = 60)]
fn build_page(config: Config) -> String {
    config.tmpl.render(
        "404.html",
        NotFoundTemplate {
            navbar: NavBar::default(),
            www: get_url(config.fc.clone()),
            onion: config.fc.onion,
        },
        InfosPage {
            desc: Some("Une page perdu du web".into()),
            ..InfosPage::default()
        },
        None,
    )
}
