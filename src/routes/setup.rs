use actix_web::{get, Responder};

#[get("/setup")]
pub async fn page() -> impl Responder {
    // Explication de l'histoire de par exemple wiki/cat et le follow up
    // avec les futures video youtube probablement un shortcut
    // vers un billet de blog
    actix_web::web::Redirect::to("/")
}
