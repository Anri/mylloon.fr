use actix_web::{get, web, HttpRequest, Responder};
use cached::proc_macro::cached;
use ramhorns::Content;

use crate::{
    config::Config,
    template::{InfosPage, NavBar},
    utils::{
        markdown::{File, FilePath},
        metadata::MType,
        misc::{lang, make_kw, read_file, read_file_fallback, Html, Lang},
    },
};

#[get("/")]
pub async fn page(req: HttpRequest, config: web::Data<Config>) -> impl Responder {
    Html(build_page(config.get_ref().to_owned(), lang(req.headers())))
}

#[derive(Content, Debug)]
struct IndexTemplate {
    navbar: NavBar,
    name: String,
    pronouns: Option<String>,
    file: Option<File>,
    avatar: String,
    avatar_caption: String,
    avatar_style: StyleAvatar,
}

#[derive(Content, Debug, Default)]
struct StyleAvatar {
    round: bool,
    square: bool,
}

#[cached(time = 60)]
fn build_page(config: Config, lang: Lang) -> String {
    let (mut file, html_lang) = read_file_fallback(
        FilePath {
            base: config.locations.data_dir.clone(),
            path: "index.md".to_owned(),
        },
        MType::Index,
        &lang,
    );

    // Default values
    let mut name = config.fc.fullname.clone().unwrap_or_default();
    let mut pronouns = None;
    let mut avatar = "/icons/apple-touch-icon.png".to_owned();
    let mut avatar_caption = "EWP avatar".to_owned();
    let mut avatar_style = StyleAvatar {
        round: true,
        square: false,
    };

    if let Some(f) = &file {
        if let Some(m) = &f.metadata.info.index {
            name = m.name.clone().unwrap_or(name);
            avatar = m.avatar.clone().unwrap_or(avatar);
            m.pronouns.clone_into(&mut pronouns);
            avatar_caption = m.avatar_caption.clone().unwrap_or(avatar_caption);

            if let Some(style) = m.avatar_style.clone() {
                if style.trim() == "square" {
                    avatar_style = StyleAvatar {
                        square: true,
                        ..StyleAvatar::default()
                    }
                }
            }
        }
    } else {
        file = read_file(
            FilePath {
                base: String::new(),
                path: "README.md".to_owned(),
            },
            MType::Generic,
            None,
        );
    }

    config.tmpl.render(
        "index.html",
        IndexTemplate {
            navbar: NavBar {
                index: true,
                ..NavBar::default()
            },
            file,
            name,
            pronouns,
            avatar,
            avatar_caption,
            avatar_style,
        },
        InfosPage {
            title: config.fc.fullname,
            desc: Some("Page principale".into()),
            kw: Some(make_kw(&["index", "étudiant", "accueil"])),
        },
        Some(html_lang),
    )
}
