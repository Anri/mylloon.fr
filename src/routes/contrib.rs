use crate::{
    config::Config,
    logic::contrib::{fetch, Project},
    template::{InfosPage, NavBar},
    utils::misc::{make_kw, Html},
};
use actix_web::{get, web, Responder};
use cached::proc_macro::once;
use ramhorns::Content;

#[get("/contrib")]
pub async fn page(config: web::Data<Config>) -> impl Responder {
    Html(build_page(config.get_ref().to_owned()).await)
}

#[derive(Content, Debug)]
struct PortfolioTemplate {
    navbar: NavBar,
    error: bool,
    projects: Option<Vec<Project>>,
    waiting: Option<Vec<Project>>,
    closed: Option<Vec<Project>>,
}

#[once(time = 600)] // 10min
async fn build_page(config: Config) -> String {
    let navbar = NavBar {
        contrib: true,
        ..NavBar::default()
    };

    // Fetch latest data from github
    let data = match fetch().await {
        Ok(data) => PortfolioTemplate {
            navbar,
            error: false,
            projects: Some(
                data.iter()
                    .filter(|&p| !p.pulls_merged.is_empty())
                    .cloned()
                    .collect(),
            ),
            waiting: Some(
                data.iter()
                    .filter(|&p| !p.pulls_open.is_empty())
                    .cloned()
                    .collect(),
            ),
            closed: Some(
                data.iter()
                    .filter(|&p| !p.pulls_closed.is_empty())
                    .cloned()
                    .collect(),
            ),
        },
        Err(e) => {
            eprintln!("{e}");

            PortfolioTemplate {
                navbar,
                error: true,
                projects: None,
                waiting: None,
                closed: None,
            }
        }
    };

    config.tmpl.render(
        "contrib.html",
        data,
        InfosPage {
            title: Some("Mes contributions".into()),
            desc: Some(format!(
                "Contributions d'{} sur GitHub",
                config.fc.name.unwrap_or_default()
            )),
            kw: Some(make_kw(&[
                "github",
                "contributions",
                "open source",
                "développement",
                "portfolio",
                "projets",
                "code",
            ])),
        },
        None,
    )
}
