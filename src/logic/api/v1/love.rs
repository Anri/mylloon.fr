use std::time::Duration;

use chrono::{DateTime, Utc};
use chrono_tz::Europe;
use cyborgtime::format_duration;
use serde::Serialize;

#[derive(Serialize)]
pub struct Info {
    unix_epoch: i64,
    date: String,
    since: String,
}

pub fn json() -> Info {
    let target = 1_605_576_600;
    let current_time = Utc::now().timestamp();

    let date = DateTime::from_timestamp(target, 0)
        .unwrap()
        .with_timezone(&Europe::Paris)
        .to_rfc2822();

    Info {
        unix_epoch: target,
        date,
        since: {
            let duration_epoch = current_time - target;
            let duration = Duration::from_secs(duration_epoch.try_into().unwrap());
            format_duration(duration).to_string()
        },
    }
}
