use std::time::Duration;

use chrono::{DateTime, Utc};
use chrono_tz::Europe;
use cyborgtime::format_duration;
use serde::Serialize;

#[derive(Serialize)]
pub struct Info {
    unix_epoch: i64,
    departure: String,
    arrival: String,
    countdown: String,
    since: String,
}

pub fn json() -> Info {
    let target = 1_736_616_600;
    let start = 1_724_832_000;
    let current_time = Utc::now().timestamp();

    let departure = DateTime::from_timestamp(start, 0)
        .unwrap()
        .with_timezone(&Europe::Paris)
        .to_rfc2822();
    let arrival = DateTime::from_timestamp(target, 0)
        .unwrap()
        .with_timezone(&Europe::Paris)
        .to_rfc2822();

    if current_time > target {
        Info {
            unix_epoch: target,
            departure,
            arrival,
            countdown: "Already happened".to_owned(),
            since: "Not relevant anymore".to_owned(),
        }
    } else {
        Info {
            unix_epoch: target,
            departure,
            arrival,
            countdown: {
                let duration_epoch = target - current_time;
                let duration = Duration::from_secs(duration_epoch.try_into().unwrap());
                format_duration(duration).to_string()
            },
            since: {
                let duration_epoch = current_time - start;
                let duration = Duration::from_secs(duration_epoch.try_into().unwrap());
                format_duration(duration).to_string()
            },
        }
    }
}
