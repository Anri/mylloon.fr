use cached::proc_macro::once;
use ramhorns::Content;

#[derive(Content, Debug, Clone)]
pub struct Language {
    pub name: String,
    code: String,
    color: String,
    sort: u8,
}

impl Language {
    pub fn new(name: &str, code: &str, color: &str, sort: Option<u8>) -> Language {
        Language {
            name: name.into(),
            code: code.into(),
            color: color.into(),
            sort: sort.unwrap_or(0),
        }
    }
}

#[once]
pub fn get_langs() -> Vec<Language> {
    // Sorting:
    // Order is from 1 to 255, equality will fallback to name
    let mut langs = vec![
        Language::new("TypeScript", "ts", "3178c6", Some(1)),
        Language::new("JavaScript", "js", "f1e05a", Some(1)),
        Language::new("Python", "py", "3572a5", Some(2)),
        Language::new("Rust", "rust", "dea584", None),
        Language::new("OCaml", "ocaml", "ef7a08", None),
        Language::new("Go", "go", "00add8", None),
        Language::new("GDScript", "gdscript", "355570", None),
        Language::new("TeX", "tex", "3d6117", None),
        Language::new("C", "c", "555555", None),
        Language::new("C++", "cpp", "f34b7d", None),
        Language::new("Zig", "zig", "ec915c", None),
        Language::new("Vue", "vue", "41b883", None),
        Language::new("Kotlin", "kotlin", "a97bff", None),
        Language::new("Java", "java", "b07219", None),
    ];

    langs.sort_by(|a, b: &Language| match (a.sort, b.sort) {
        (0, 0) => a.name.cmp(&b.name),
        (0, k) if (k > 0) => std::cmp::Ordering::Greater,
        (k, 0) if (k > 0) => std::cmp::Ordering::Less,
        (f, s) if (f == s) => a.name.cmp(&b.name),
        (f, s) => f.cmp(&s),
    });

    langs
}
