use std::{cmp::Ordering, path::Path};

use cached::proc_macro::once;
use regex::Regex;
use serde::Serialize;

#[derive(Clone, Debug, Serialize, PartialEq, Eq)]
pub struct FileNode {
    name: String,
    is_dir: bool,
    children: Vec<FileNode>,
}

impl Ord for FileNode {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self.is_dir, other.is_dir) {
            // If both are directories or both are files, compare names
            (true, true) | (false, false) => self.name.cmp(&other.name),
            // If self is directory and other is file, self comes first
            (true, false) => Ordering::Less,
            // If self is file and other is directory, other comes first
            (false, true) => Ordering::Greater,
        }
    }
}

impl PartialOrd for FileNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[once(time = 600)]
pub fn get_filetree(
    initial_dir: &str,
    exclusion_list: &[String],
    exclusion_patterns: &[Regex],
) -> FileNode {
    gen_filetree(initial_dir, exclusion_list, exclusion_patterns).unwrap_or(FileNode {
        name: String::new(),
        is_dir: false,
        children: vec![],
    })
}

fn gen_filetree(
    dir_path: &str,
    exclusion_list: &[String],
    exclusion_patterns: &[Regex],
) -> Result<FileNode, std::io::Error> {
    let mut children: Vec<FileNode> = std::fs::read_dir(dir_path)?
        .filter_map(Result::ok)
        .filter_map(|entry| {
            let entry_path = entry.path();
            let entry_name = entry_path.file_name()?.to_string_lossy().to_string();

            // Exclusion checks
            if excluded(&entry_name, exclusion_list, exclusion_patterns) {
                return None;
            }

            if entry_path.is_file() {
                Some(FileNode {
                    name: entry_name,
                    is_dir: false,
                    children: vec![],
                })
            } else {
                // Exclude empty directories
                let children_of_children = gen_filetree(
                    entry_path.to_str().unwrap(),
                    exclusion_list,
                    exclusion_patterns,
                );
                if let Ok(coc) = children_of_children {
                    if coc.is_dir && coc.children.is_empty() {
                        None
                    } else {
                        Some(coc)
                    }
                } else {
                    None
                }
            }
        })
        .collect();

    children.sort();

    Ok(FileNode {
        name: Path::new(dir_path)
            .file_name()
            .unwrap()
            .to_string_lossy()
            .to_string(),
        is_dir: true,
        children,
    })
}

pub fn excluded(element: &str, exclusion_list: &[String], exclusion_patterns: &[Regex]) -> bool {
    if exclusion_list
        .iter()
        .any(|excluded_term| element.contains(excluded_term))
    {
        return true;
    }
    if exclusion_patterns.iter().any(|re| re.is_match(element)) {
        return true;
    }

    false
}
