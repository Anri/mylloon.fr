use cached::proc_macro::once;
use glob::glob;
use std::fs::read_to_string;

use crate::utils::{
    markdown::{File, FilePath},
    metadata::MType,
    misc::read_file,
};

/// Contact node
#[derive(Clone, Debug)]
pub struct Link {
    pub service: String,
    pub scope: Option<String>,
    pub url: String,
}

#[once(time = 60)]
pub fn find_links(directory: String) -> Vec<Link> {
    // TOML filename
    let toml_file = "links.toml";

    // Read the TOML file and parse it
    let toml_str = read_to_string(format!("{directory}/{toml_file}")).unwrap_or_default();

    let mut redirections = vec![];
    match toml::de::from_str::<toml::Value>(&toml_str) {
        Ok(data) => {
            if let Some(section) = data.as_table() {
                section.iter().for_each(|(key, value)| {
                    // Scopes are delimited  with `/`
                    let (service, scope) = match key.split_once('/') {
                        Some((service, scope)) => (service.to_owned(), Some(scope.to_owned())),
                        None => (key.to_owned(), None),
                    };

                    redirections.push(Link {
                        service,
                        scope,
                        url: value.as_str().unwrap().to_owned(),
                    });
                });
            }
        }
        Err(_) => return vec![],
    }

    redirections
}

pub fn remove_paragraphs(list: &mut [File]) {
    list.iter_mut()
        .for_each(|file| file.content = file.content.replace("<p>", "").replace("</p>", ""));
}

pub fn read(path: &FilePath) -> Vec<File> {
    glob(&path.to_string())
        .unwrap()
        .map(|e| {
            read_file(
                path.from(&e.unwrap().to_string_lossy()),
                MType::Contact,
                None,
            )
            .unwrap()
        })
        .filter(|f| {
            !f.metadata
                .info
                .contact
                .clone()
                .unwrap()
                .hide
                .unwrap_or_default()
        })
        .collect::<Vec<File>>()
}
