use actix_files::Files;
use actix_web::{
    middleware::{Compress, DefaultHeaders, Logger},
    web, App, HttpServer,
};
use std::io::Result;

use crate::routes::{
    agreements, api_v1, blog, contact, contrib, cours, cv, gaming, index, memorial, not_found,
    portfolio, setup, statik, web3,
};

mod config;
mod template;

mod logic;
mod routes;
mod utils;

#[actix_web::main]
async fn main() -> Result<()> {
    let config = config::get_configuration("./config/config.toml");

    let addr = (
        if cfg!(debug_assertions) {
            "127.0.0.1"
        } else {
            "0.0.0.0"
        },
        config.fc.port.unwrap(),
    );

    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(config.clone()))
            .wrap(
                Logger::new("%ra \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\" %T")
                    .log_target(config.fc.app_name.clone().unwrap_or_default()),
            )
            .wrap(Compress::default())
            .wrap(
                DefaultHeaders::new()
                    .add((
                        "Onion-Location",
                        config.fc.onion.as_deref().unwrap_or_default(),
                    ))
                    .add(("Server", format!("ewp/{}", env!("CARGO_PKG_VERSION"))))
                    .add(("Permissions-Policy", "interest-cohort=()"))
                    .add((
                        "Content-Security-Policy",
                        "script-src 'self' 'unsafe-inline' *",
                    ))
                    .add(("X-Frame-Options", "SAMEORIGIN"))
                    .add(("X-Content-Type-Options", "nosniff"))
                    .add(("Referrer-Policy", "no-referrer")),
            )
            .service(
                web::scope("/api").service(
                    web::scope("v1")
                        .service(api_v1::love)
                        .service(api_v1::btf)
                        .service(api_v1::websites),
                ),
            )
            .service(index::page)
            .service(statik::languages)
            .service(agreements::security)
            .service(agreements::humans)
            .service(agreements::robots)
            .service(agreements::webmanifest)
            .service(blog::index)
            .service(blog::rss)
            .service(blog::page)
            .service(contrib::page)
            .service(cours::page)
            .service(cv::page)
            .service(gaming::page)
            .service(memorial::page)
            .configure(contact::pages)
            .service(portfolio::page)
            .service(setup::page)
            .service(web3::page)
            .service(Files::new("/", config.locations.static_dir.clone()))
            .default_service(web::to(not_found::page))
    })
    .bind(addr)?
    .run()
    .await
}
